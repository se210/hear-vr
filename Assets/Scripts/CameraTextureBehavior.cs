﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraTextureBehavior : MonoBehaviour {
	RawImage BackgroundTexture;
	WebCamTexture CameraTexture;

	public RectTransform SpeechCanvasRectTransform;

	// Use this for initialization
	void Start () {
		BackgroundTexture = gameObject.AddComponent<RawImage>();
		Debug.Log ("Screen size: width= " + Screen.width.ToString () + " height=" + Screen.height.ToString ());
//		float FoV = Camera.main.fieldOfView;
//		float FoV = 97.49328f;
//		float d = BackgroundTexture.transform.position.z;
//		float yScale = d * Mathf.Cos (Mathf.Deg2Rad * FoV / 2.0f);
//		float xScale = yScale * Camera.main.aspect;

		//set up camera
		WebCamDevice[] devices = WebCamTexture.devices;
		Debug.Log ("Initializing camera... Found " + devices.Length.ToString () + " devices.");
		string backCamName="";
		foreach (WebCamDevice device in devices)
		{
			Debug.Log("Device: "+device.name+ ", isFrontFacing: "+device.isFrontFacing);

			if (!device.isFrontFacing) {
				backCamName = device.name;
			}
		}

		CameraTexture = new WebCamTexture(backCamName, 10000, 10000, 60);
		CameraTexture.Play();
		BackgroundTexture.texture = CameraTexture;

		StartCoroutine("setAfterDelay");
	}

	IEnumerator setAfterDelay()
	{
		// Wait for camera to initialize to get the correct camera info
		yield return new WaitForSeconds(1.0f);

		Debug.Log ("Setting BackgroundTexture position and scale.");
		// If video is vertically mirrored, flip it upside down
		float scaleY = CameraTexture.videoVerticallyMirrored ? -1.0f : 1.0f;

		// Scale camera according to its size
		Debug.Log ("Camera size: width= " + CameraTexture.width.ToString () + " height=" + CameraTexture.height.ToString ());
		BackgroundTexture.transform.localPosition = new Vector3 (0.0f, 0.0f, CameraTexture.height * 0.6f);
		BackgroundTexture.transform.localScale = new Vector3(CameraTexture.width * 0.5f, CameraTexture.height * 0.5f, 0.0f);
		Vector3 localScale = BackgroundTexture.transform.localScale;
		BackgroundTexture.transform.localScale = new Vector3(localScale.x, localScale.y * scaleY, 0.0f);

		Rect myRect = gameObject.GetComponent<RectTransform> ().rect;
		SpeechCanvasRectTransform.transform.localPosition = BackgroundTexture.transform.localPosition;
		SpeechCanvasRectTransform.sizeDelta = new Vector2 (localScale.x, localScale.y);

		Debug.Log ("localPosition: " + BackgroundTexture.transform.localPosition.ToString ());
		Debug.Log ("localScale: " + BackgroundTexture.transform.localScale.ToString ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
